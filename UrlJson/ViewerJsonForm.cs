﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace UrlJson
{
    public partial class ViewerJsonForm : Form
    {
        
        public ViewerJsonForm()
        {
            InitializeComponent();
        }

        private void ViewerJsonForm_Load(object sender, EventArgs e)
        {
            WebClient client = new WebClient();

            string strWebCode = client.DownloadString(@"https://jsonplaceholder.typicode.com/comments?postId=1");

            txtWebJson.Text = strWebCode;

            dynamic dyn = JsonConvert.DeserializeObject<dynamic>(strWebCode);

            // postId
            string postId_1 = dyn[0]["postId"].ToString();
            // id
            string id_0 = dyn[0]["id"].ToString();
            string id_1 = dyn[1]["id"].ToString();
            string id_2 = dyn[2]["id"].ToString();
            string id_3 = dyn[3]["id"].ToString();
            string id_4 = dyn[4]["id"].ToString();
            // name
            string name_0 = dyn[0]["name"].ToString();
            string name_1 = dyn[1]["name"].ToString();
            string name_2 = dyn[2]["name"].ToString();
            string name_3 = dyn[3]["name"].ToString();
            string name_4 = dyn[4]["name"].ToString();


            txtResult.Text = "For the attribute (key) postId: " + postId_1;
            txtResult.Text += System.Environment.NewLine;

            txtResult.Text += System.Environment.NewLine + "id: " + id_0;
            txtResult.Text += System.Environment.NewLine + "name: " + name_0;
            txtResult.Text += System.Environment.NewLine;
            txtResult.Text += System.Environment.NewLine + "id: " + id_1;
            txtResult.Text += System.Environment.NewLine + "name: " + name_1;
            txtResult.Text += System.Environment.NewLine;
            txtResult.Text += System.Environment.NewLine + "id: " + id_2;
            txtResult.Text += System.Environment.NewLine + "name: " + name_1;
            txtResult.Text += System.Environment.NewLine;
            txtResult.Text += System.Environment.NewLine + "id: " + id_3;
            txtResult.Text += System.Environment.NewLine + "name: " + name_1;
            txtResult.Text += System.Environment.NewLine;
            txtResult.Text += System.Environment.NewLine + "id: " + id_4;
            txtResult.Text += System.Environment.NewLine + "name: " + name_1;



        }
    }
}

// * * *

// string output = JsonConvert.DeserializeObject<string>(strWebCode);

// dynamic dyn = JsonConvert.DeserializeObject<dynamic>(strWebCode);
